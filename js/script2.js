// 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення,
// скільки днів у цьому місяці. Результат виводиться в консоль.(Використайте switch case)

let month = prompt ('Введіть назву місяця маленькими літерами');

switch (month){
    case 'січень':
        console.log('В січні 31 день');
        break;
    case 'лютий':
        console.log('В лютому 28 днів');
        break
    case 'березень':
        console.log('В березні 31 день');
        break
    case 'квітень':
        console.log('В квітні 30 днів');
        break    
    case 'травень':
        console.log('В травні 31 день');
        break
    case 'червень':
        console.log('В червні 30 днів');
        break
    case 'липень':
        console.log('В липні 31 день');
        break
    case 'серпень':
        console.log('В серпні 31 день');
        break
    case 'вересень':
        console.log('В вересні 30 днів');
        break
    case 'жовтень':
        console.log('В жовтні 31 день');
        break
    case 'листопад':
        console.log('В листопаді 30 днів');
        break
    case 'грудень':
        console.log('В грудні 31 день');
        break
    default:
        console.log('Такого місяця не існує');
        break
}